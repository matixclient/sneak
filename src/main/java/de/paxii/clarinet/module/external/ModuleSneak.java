package de.paxii.clarinet.module.external;

import de.paxii.clarinet.Wrapper;
import de.paxii.clarinet.event.EventHandler;
import de.paxii.clarinet.event.events.player.PostMotionUpdateEvent;
import de.paxii.clarinet.event.events.player.PreMotionUpdateEvent;
import de.paxii.clarinet.module.Module;
import de.paxii.clarinet.module.ModuleCategory;
import java.util.LinkedList;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import net.minecraft.network.play.client.CPacketEntityAction;
import net.minecraft.network.play.client.CPacketEntityAction.Action;

/**
 * Created by Lars on 05.07.17.
 */
public class ModuleSneak extends Module {

  public ModuleSneak() {
    super("Sneak", ModuleCategory.MOVEMENT);

    this.setVersion("1.0");
    this.setBuildVersion(18200);
    this.setDescription("Packet Sneak, duh");
    this.setRegistered(true);
  }

  @EventHandler
  public void onPreMotionUpdate(PreMotionUpdateEvent preMotionUpdateEvent) {
    this.sendPackets(false);
  }

  @EventHandler
  public void onPostMotionUpdate(PostMotionUpdateEvent postMotionUpdateEvent) {
    this.sendPackets(true);
  }

  private void sendPackets(boolean reverse) {
    LinkedList<Action> actions = Stream
        .of(Action.START_SNEAKING, Action.STOP_SNEAKING)
        .collect(Collectors.toCollection(LinkedList::new));

    (reverse ? actions.descendingIterator() : actions.iterator()).forEachRemaining(action ->
        Wrapper.getSendQueue().addToSendQueue(new CPacketEntityAction(Wrapper.getPlayer(), action))
    );
  }

  @Override
  public void onDisable() {
    Wrapper.getSendQueue().addToSendQueue(
        new CPacketEntityAction(Wrapper.getPlayer(), Action.STOP_SNEAKING)
    );
  }

}
